Solutions to 

Task 1- Keep the dockerfile in the project repo and build the image

Task 2- Keep the dockerfile and docker-compose.yml file in project dir and do docker-compose up and check at localhost:8080 whether app is running or not.
Create a redis and kafka directory in the home directory for persistent volume mount

Task 3 - Configure aws access keys using aws configure and then enter the terraform directory and do terraform init and terraform apply

Assumptions:
redis is running at redis1.com
kafka is running at kafka1.com
FROM golang:latest
COPY . /go/src
WORKDIR /go/src
EXPOSE 8080
ENTRYPOINT ["./start.sh"]
